class Job {
    name = null
    steps = []
    runResults = null

    constructor(name = 'Job') {
        this.name = name
    }

    append(step) {
        this.steps.push(step)

        return this
    }

    run() {
        let success = 0
        let errors = 0

        this.steps.forEach((step, i) => {
            console.log(`STEP ${i + 1}: ${step.name}`)
            let bool = step.execute()

            if (bool === true) {
                success++
            } else {
                errors++
            }
            
            console.log(``)
        })

        this.runResults = { success, errors }

        return this
    }

    printResults() {
        if (this.runResults === null) {
            console.error(`${this.name} not ran!`)
            return
        }

        console.log(`${this.name} results`)
        console.log(`Success: ${this.runResults.success}`)
        console.log(`Errors: ${this.runResults.errors}`)
        console.log(``)
    }
}

module.exports = Job