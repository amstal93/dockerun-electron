const path = require('path')
const fs = require('fs')
const shell = require('shelljs')
const del = require('del');

const Job = require('./Job')
const Step = require('./Step')

const job = new Job('Dockerun Electron builder')

const config = {
    GIT_REPO: 'https://gitlab.com/fsorge/dockerun-frontend.git/',
    GIT_BRANCH: 'main'
}

const cleanup = new Step('Cleanup...', () => {
    del.sync(path.join(__dirname, '..', 'dockerun-frontend'))
    del.sync(path.join(__dirname, '..', 'dist'))
    del.sync(path.join(__dirname, '..', 'app'))

    return true
})

const gitClone = new Step('Downloading Dockerun', () => {
    shell.cd(path.join(__dirname, '..'))
    shell.exec(`git clone -b ${config.GIT_BRANCH} ${config.GIT_REPO}`)

    return true
})

const npm = new Step('npm install & build production (may take several minutes...)', () => {
    shell.cd(path.join(__dirname, '..', 'dockerun-frontend'))
    shell.exec('npm i')
    shell.exec('npm run build')

    return true
})

const moves = new Step('Moving folders', () => {
    fs.renameSync(path.join(__dirname, '..', 'dockerun-frontend', 'dist'), path.join(__dirname, '..', 'app'))

    return true
})

const finalCleanup = new Step('Final cleanup', () => {
    del.sync(path.join(__dirname, '..', 'dockerun-frontend'), { force: true })

    return true
})

job
.append(cleanup)
.append(gitClone)
.append(npm)
.append(moves)
.append(finalCleanup) // BUG This step doesn't work
.run()
.printResults()